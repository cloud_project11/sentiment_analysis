import googlemaps
import csv

def getInfo():
	infoList = []
	with open('csvfiles/psychological_distress_aurin.csv', 'r') as f:
		data = csv.DictReader(f)
		for row in data:
			tmpDict = {}
			tmpDict['postcode'] = getPostcode(row[' area_name'])
			tmpDict['area'] = row[' area_name']
			tmpDict['distress_rate'] = row[' k10_me_2_rate_3_11_7_13']
			tmpDict['distress_count'] = row['k10_me_1_no_3_11_7_13']

			infoList.append(tmpDict)
			print(tmpDict)

	return infoList

def outputCSV(infoList):
	csv_columns = ['postcode', 'area', 'distress_rate', 'distress_count']
	with open('csvfiles/aurin_psychological_distress_output.csv', 'w') as csvfile:
		writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
		writer.writeheader()
		for data in infoList:
			writer.writerow(data)

def getPostcode(address):
	API = 'AIzaSyDtjwyX7qLWTAABDvw63W5m-hIb3nHduhQ'
	gmap = googlemaps.Client (API)
	res = gmap.geocode(address, region="AU")
	info_place = gmap.reverse_geocode(res[0]["geometry"]["location"])
	for list in info_place:
		for data in list['address_components']:
			if data['types'] == ['postal_code']:
				return data['long_name']

outputCSV(getInfo())
