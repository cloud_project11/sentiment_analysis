from tweepy import OAuthHandler
import tweepy
import time
import couchdb
import sys
from process_tweet import get_sentiment

server1 = {
    # vero_tp credentials "My app for Australia"
    'credentials': {
        'ckey': "KlyX6o09eQqVit5n4MjDoa0G9",
        'csecret': "pAcZ44okpHfhzU1cjKQbtStQq1vuvisOjmh9Pp3ROzZnDwq0Wi",
        'atoken': "67933571-XZxhZmTe9i6YccUM2CPN5i77YoA3dKKOye072vWqd",
        'asecret': "FXWQGVJhplNIFOhrPw1eDeDQCOJtc9P7edzygH92crgXo"
    },
}

server = server1

db_name = 'aus_analytics'

couch = couchdb.Server('http://115.146.93.17:5984')
db = couch[db_name]

class TweetProcessor:

    def process_tweet(self, status):
        global db
        json_tweet = status._json
        json_tweet["_id"] = json_tweet["id_str"]
        json_tweet["sentiment"] = get_sentiment(json_tweet["text"])
        json_tweet["type"] = 'tweet_user'
        retry = False
        while True:
            try:
                if retry:
                    couch = couchdb.Server('http://115.146.93.17:5984')
                    db = couch[db_name]
                db.save(json_tweet)
                return True
            except couchdb.http.ResourceConflict:
                return False
            except couchdb.http.ServerError as e:
                print("Server error %s. Sleeping for 60 seconds" % e)
                sys.stdout.flush()
                time.sleep(60)
                retry = True

    def process_tweets(self, results):
        last_id = int(results[0].id_str)
        processed = 0
        for res in results:
            if tp.process_tweet(res):
                processed += 1
            if int(res.id_str) < last_id:
                last_id = int(res.id_str)
        return last_id, processed


auth = OAuthHandler(server['credentials']['ckey'], server['credentials']['csecret'])
auth.set_access_token(server['credentials']['atoken'], server['credentials']['asecret'])
api = tweepy.API(auth)

print("start")
sys.stdout.flush()

e_user_ids = []

for e_user in db.view('helper/existing_users', group=True):
    e_user_ids.append(e_user.key)

user_ids = []

for geo_tweet in db.view('filtered/has_geo'):
    user = geo_tweet.value['user']
    if user['geo_enabled']:
        user_ids.append(user['id'])

user_ids = list(set(user_ids) - set(e_user_ids))

for u_id in user_ids:

    tp = TweetProcessor()

    count = 0
    while True:
        try:
            results = api.user_timeline(id=u_id, lang='en', count=200)
            break
        except tweepy.error.RateLimitError:
            print("1. Rate limit error: sleeping for 1000 seconds")
            sys.stdout.flush()
            time.sleep(1000)
        except KeyboardInterrupt:
            exit()
        except tweepy.error.TweepError as e:
            if e.api_code == 34:
                print("2. %s: Tweepy error, skiping user" % e.api_code)
                sys.stdout.flush()
                results = []
                break
            elif "Not authorized." == str(e):
                print("2. %s: Another tweepy error, skiping user" % e)
                sys.stdout.flush()
                results = []
                break
            else:
                print("2. %s: Unknown Tweepy error sleeping for 1000 seconds" % e)
                sys.stdout.flush()
                time.sleep(1000)
        except Exception as e:
            print("1. Unknown Error %s - Sleeping for 1000 seconds " % e)
            sys.stdout.flush()
            time.sleep(1000)

    while len(results) > 0:
        last_id, processed = tp.process_tweets(results)
        count += processed
        sys.stdout.flush()
        while True:
            try:
                results = api.user_timeline(id=u_id, lang='en', count=200, max_id=last_id)
                break
            except tweepy.error.RateLimitError:
                print("2. Rate limit error: sleeping for 1000 seconds")
                sys.stdout.flush()
                time.sleep(1000)
            except KeyboardInterrupt:
                exit()
            except tweepy.error.TweepError as e:
                if e.api_code == 34:
                    print("2. %s: Tweepy error, skiping user" % e.api_code)
                    sys.stdout.flush()
                    results = []
                    break
                elif "Not authorized." == str(e):
                    print("2. %s: Another tweepy error, skiping user" % e)
                    sys.stdout.flush()
                    results = []
                    break
                else:
                    print("2. %s: Unknown Tweepy error sleeping for 1000 seconds" % e)
                    sys.stdout.flush()
                    time.sleep(1000)
            except Exception as e:
                print("2. Unknown Error %s - Sleeping for 1000 seconds" % e)
                sys.stdout.flush()
                time.sleep(1000)
        if len(results) > 0:
            results.pop(0)
    print("Collected %d for user '%s'" % (count, u_id))
    sys.stdout.flush()