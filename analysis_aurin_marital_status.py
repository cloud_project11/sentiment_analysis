import googlemaps
import csv

def getAddress():
	address_list = []
	with open('csvfiles/aurin_marital_status.csv', 'r') as f:
		data = csv.DictReader(f)
		for row in data:
			address_list.append(row[' sa2_name11'])
	return address_list

def getInfo():
	infoList = []
	with open('csvfiles/aurin_marital_status.csv', 'r') as f:
		data = csv.DictReader(f)
		for row in data:
			postcode = getPostcode(row[' sa2_name11'])
			males_married = float(row[' males_married'])
			males_married_percent = float(row[' males_married_percent'])
			females_married = float(row[' females_married'])
			females_married_percent = float(row[' females_married_percent'])
			males_divorced = float(row[' males_divorced'])
			females_divorced = float(row[' females_divorced'])
			males_never_married = float(row[' males_never_married'])
			females_never_married = float(row[' females_never_married'])
			males_count = round(males_married / males_married_percent)
			females_count = round(females_married / females_married_percent)
			people_count = males_count + females_count

			# print(males_married, females_married, males_divorced, females_divorced, males_never_married, females_never_married)
			# print(males_count, females_count, people_count)

			tmpDict = {}
			tmpDict['postcode'] = postcode
			tmpDict['address'] = row[' sa2_name11']
			tmpDict['married_percent'] = (males_married + females_married) / people_count
			tmpDict['never_married_percent'] = (males_never_married + females_never_married) / people_count
			tmpDict['divorced_percent'] = (males_divorced + females_divorced) / people_count
			tmpDict['other'] = 100 - tmpDict['married_percent'] - tmpDict['never_married_percent'] - tmpDict['divorced_percent']

			infoList.append(tmpDict)
			print(tmpDict)

	return infoList

def outputCSV(infoList):
	csv_columns = ['postcode', 'address', 'married_percent', 'never_married_percent', 'divorced_percent', 'other']
	with open('csvfiles/aurin_marital_status_output.csv', 'w') as csvfile:
		writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
		writer.writeheader()
		for data in infoList:
			writer.writerow(data)

def getPostcode(address):
	API = 'AIzaSyDtjwyX7qLWTAABDvw63W5m-hIb3nHduhQ'
	gmap = googlemaps.Client (API)
	res = gmap.geocode(address, region="AU")
	info_place = gmap.reverse_geocode(res[0]["geometry"]["location"])
	for list in info_place:
		for data in list['address_components']:
			# if not data['types'] == ['country', 'political'] and data['short_name'] == 'AU':
				# break
			if data['types'] == ['postal_code']:
				return data['long_name']

def get_postcode(address_list):
	API = 'AIzaSyDtjwyX7qLWTAABDvw63W5m-hIb3nHduhQ'
	gmap = googlemaps.Client (API)
	postcode_list = []
	# address_list = ["Point Nepean"]
	for address in address_list:
		res = gmap.geocode(address)
		info_place = gmap.reverse_geocode(res[0]["geometry"]["location"])
		# print(res[0]['geometry']['location'])
		for list in info_place:
			for data in list['address_components']:
				if data['types'] == ['postal_code']:
					postcode_list.append(data['long_name'])
					# print(data['long_name'])
	return postcode_list

outputCSV(getInfo())
