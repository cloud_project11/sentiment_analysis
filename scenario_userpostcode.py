import couchdb
import json
import operator

couch = couchdb.Server('http://115.146.93.17:5984')
dbsource = couch['aus_dev3']
# L = dbsource.view('_design/grouping/_view/user_postcode', reduce=True, group=True)
L = dbsource.view('_design/grouping/_view/user_postcode', reduce=False)

user = dict()
for row in L:
    key = row['key']
    value = row['value']
    if key[0] not in user:
        user[key[0]] = type('User', (object,), {"pos":0, "neu":0, "neg":0, "postcodes":dict(), "len":0, "words":dict()})
    user[key[0]].pos += value[0]
    user[key[0]].neu += value[1]
    user[key[0]].neg += value[2]
    user[key[0]].postcodes[key[1]] = 1

    user[key[0]].len = len(user[key[0]].postcodes)

    words = value[3].split()
    for word in words:
        if word not in user[key[0]].words:
            user[key[0]].words[word] = 0
        user[key[0]].words[word] += 1

wordcloud = dict()
sv = sorted(user, key=lambda id: user[id].len, reverse=True)
for key in sv:
    if user[key].len <= 1:
        break

    # sort the words and print out
    sortedwords = sorted(user[key].words.items(), key=lambda x:x[1], reverse=True)
    print(key, user[key].pos, user[key].neu, user[key].neg, user[key].len, sortedwords)

    #take top N words and put it in word cloud
    N = 3
    sortedwords = sortedwords[:N]
    for word in sortedwords:
        if word[0] not in wordcloud:
            wordcloud[word[0]] = 0

        wordcloud[word[0]] += 1

#sort wordclouds and print
sortedwordcloud = sorted(wordcloud.items(), key=lambda x:x[1], reverse=True)
print (sortedwordcloud)
    
