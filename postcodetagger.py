import couchdb
import json


def inPolygonCheck(polys, test):
    retVal = False
    i=0
    j=len(polys)-1

    while i<len(polys):
        if ( ((polys[i][1]>=test.y) != (polys[j][1]>=test.y)) and (test.x<=(polys[j][0]-polys[i][0])*(test.y-polys[i][1])/(polys[j][1]-polys[i][1])+polys[i][0]) ):
            retVal = not retVal
        j = i
        i += 1

    return retVal

f = open('data/Victoria_Postcode_boundaries__polygon_-_1_250_000_to_1__5_million._Vicmap_Lite.json/data2156806729017421276.json', 'r')

while 1:
    line = f.readline()
    if not line:
        break

    d = json.loads(line)

    polys = []
    for geom in d['features']:
        temp = type('Polygon', (object,), { "name": "something", "coor":[] })
        temp.name = geom['properties']['POSTCODE']
        temp.coor = geom['geometry']['coordinates'][0][0]
        polys.append(temp)

couch = couchdb.Server('http://115.146.93.17:5984')
dbsource = couch['aus_analytics']
L = dbsource.view('_design/filtered/_view/has_geo')

for row in L:
    item = type('Point', (object,), {"x": 0, "y": 0, "sentiment": 0, "location": 'unknown'})
    if ('geo' in row['value'] and row['value']['geo'] is not None):
        item.x = row['value']['geo']['coordinates'][1]
        item.y = row['value']['geo']['coordinates'][0]
    if ('sentiment' in row['value']):
        item.sentiment = row['value']['sentiment']
    else:
        continue

    for poly in polys:
        retVal = inPolygonCheck(poly.coor, item)
        if retVal:
            row['value']['postcode'] = poly.name
            # print(row)
            dbsource.save(row['value'])
            break

