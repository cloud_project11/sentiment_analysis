import couchdb
from process_tweet import get_sentiment
import time
import sys

db_name = 'aus_analytics'

couch = couchdb.Server('http://115.146.93.17:5984')
db = couch[db_name]


print('started')
sys.stdout.flush()
options = {'limit': 50000}

while True:
    count = 0
    for emp_match in db.view('helper_p/no_sent', **options):
        if count % 100 == 0:
            print("Processed %d tweets" % count)
            sys.stdout.flush()
        emp_match.value['sentiment'] = get_sentiment(emp_match.value['text'])
        if count % 10 == 0:
            print("Tagged with: %s" % emp_match.value['sentiment'])
            sys.stdout.flush()
        db.save(emp_match.value)
        retry = False
        while True:
            try:
                if retry:
                    couch = couchdb.Server('http://115.146.93.17:5984')
                    db = couch[db_name]
                db.save(emp_match.value)
                break
            except couchdb.http.ServerError as e:
                print("Server error %s. Sleeping for 60 seconds" % e)
                sys.stdout.flush()
                time.sleep(60)
                retry = True
        count += 1
    if count == 0:
        break
print("Finished")
