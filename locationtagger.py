import couchdb
import shapefile
import json


def inPolygonCheck(polys, test):
    retVal = False
    i=0
    j=len(polys)-1

    while i<len(polys):
        if ( ((polys[i][1]>=test.y) != (polys[j][1]>=test.y)) and (test.x<=(polys[j][0]-polys[i][0])*(test.y-polys[i][1])/(polys[j][1]-polys[i][1])+polys[i][0]) ):
            retVal = not retVal
        j = i
        i += 1

    return retVal

# a = [[1, 20], [10, 20], [10, 3], [1, 3]]
# b = type('Point', (object,), {"x": 10, "y": 10})
# print(inPolygonCheck(a, b))

# sf = shapefile.Reader("blackspots/98a94386-6975-42b4-97db-bc09f3a9805c")
# sf = shapefile.Reader("data/Victorian_Features_of_Interest__Polygon__-_Vicmap_FOI/shp/242af68e-b541-4548-ad12-deb4f2102f57")
f = open('data/Victorian_Features_of_Interest__Polygon__-_Vicmap_FOI.json/data8013966448269344642.json', 'r')

while 1:
    line = f.readline()
    if not line:
        break

    d = json.loads(line)

    # print (d['features'][0]['geometry'].keys())
    # print (d['features'][0]['properties'].keys())
    # print()

# (sport facility:*), (reserve:park, city square), (recreational resource:*)
    polys = []
    for geom in d['features']:
        # print (geom['properties']['boundedBy'])
        # print (geom['geometry']['coordinates'][0][0])
        # print (geom['properties']['FEATURE_TYPE'], geom['properties']['FEATURE_SUBTYPE'])
        # if (geom['properties']['FEATURE_TYPE'] == 'sport facility' or geom['properties']['FEATURE_TYPE'] == 'recreational resource'):
            temp = type('Polygon', (object,), { "name": "something", "type": "something", "subtype": "something", "coor":[] })
            temp.name = geom['properties']['PARENT_NAME'] , geom['properties']['NAME'] , geom['properties']['NAME_LABEL']
            temp.type = geom['properties']['FEATURE_TYPE']
            temp.subtype = geom['properties']['FEATURE_SUBTYPE']
            temp.coor = geom['geometry']['coordinates'][0][0]
            # half = len(geom['properties']['boundedBy'])/2
            # temp.coor = (geom['properties']['boundedBy'][half:],geom['properties']['boundedBy'][:half])
            polys.append(temp)


# shapes = sf.shapes()

# print (dir(shapes[0]))
# for s in shapes:
#     print('#', s.__doc__, s.__geo_interface__, s.__init__, s.__module__)
#     print(s.points, len(s.points))
#     print(s.parts)
#     print(s.bbox)

couch = couchdb.Server('http://115.146.93.17:5984')
dbsource = couch['aus_dev2']
L = dbsource.view('_design/filtered/_view/has_geo')
# L = dbsource.view('_design/filtered/_view/has_geo_and_matched')
counter = 0
rcounter = 0
items = []
# for row in L:
for id in dbsource:
    row = dbsource[id]
    # print(row['sentiment'])
    item = type('Point', (object,), {"x": 0, "y": 0, "sentiment": 0, "location": 'unknown'})
    # item.x = row['value'][0]['coordinates'][1]
    # item.y = row['value'][0]['coordinates'][0]
    if ('geo' in row and row['geo'] is not None):
        item.x = row['geo']['coordinates'][1]
        item.y = row['geo']['coordinates'][0]
    if ('sentiment' in row):
        item.sentiment = row['sentiment']
    else:
        print(row)
        continue
    rcounter += 1
    for poly in polys:
        retVal = inPolygonCheck(poly.coor, item)
        if retVal:
            counter += 1
            # print(counter, rcounter, retVal, poly.type, poly.subtype, poly.name)
            item.location = poly.type + ':' + poly.subtype + ':' + str(poly.name)
            break
    items.append(item)
    # print(rcounter, counter)

itemgrouppos = dict()
itemgroupneg = dict()
itemgroupneut = dict()
countneut = 0
countpos = 0
countneg = 0
for item in items:
    if item.location not in itemgrouppos:
        itemgrouppos[item.location] = 0
        itemgroupneg[item.location] = 0
        itemgroupneut[item.location] = 0
    if item.sentiment == 'negative':
        itemgroupneg[item.location] += 1
        countneg += 1
    elif item.sentiment == 'neutral':
        itemgroupneut[item.location] += 1
        countneut += 1
    else:
        itemgrouppos[item.location] += 1
        countpos += 1

for key in itemgrouppos.keys():
    print (key, 'pos', itemgrouppos[key], 'neg', itemgroupneg[key], 'neut', itemgroupneut[key])
print('pos', countpos, 'neg', countneg, 'neut', countneut)




