import couchdb
import json


def inPolygonCheck(polys, point):
    retVal = False
    i=0
    j=len(polys)-1

    while i<len(polys):
        if ( ((polys[i][1]>=point['y']) != (polys[j][1]>=point['y'])) and (point['x']<=(polys[j][0]-polys[i][0])*(point['y']-polys[i][1])/(polys[j][1]-polys[i][1])+polys[i][0]) ):
            retVal = not retVal
        j = i
        i += 1

    return retVal


def get_postcode(lat, lon):
    f = open('data/Victoria_Postcode_boundaries__polygon_-_1_250_000_to_1__5_million._Vicmap_Lite.json/data2156806729017421276.json', 'r')

    while 1:
        line = f.readline()
        if not line:
            break

        d = json.loads(line)

        polys = []
        for geom in d['features']:
            temp = type('Polygon', (object,), { "name": "something", "coor":[] })
            temp.name = geom['properties']['POSTCODE']
            temp.coor = geom['geometry']['coordinates'][0][0]
            polys.append(temp)


    for poly in polys:
        retVal = inPolygonCheck(poly.coor, {'x': lon, 'y': lat})
        if retVal:
            return poly.name
    return False


def tag_geos():
    couch = couchdb.Server('http://115.146.93.17:5984')
    dbsource = couch['aus_dev2']

    for id in dbsource:
        row = dbsource[id]
        item = type('Point', (object,), {"x": 0, "y": 0, "sentiment": 0, "location": 'unknown'})
        if ('geo' in row and row['geo'] is not None):
            item.x = row['geo']['coordinates'][1]
            item.y = row['geo']['coordinates'][0]
        if ('sentiment' in row):
            item.sentiment = row['sentiment']
        else:
            continue

        retVal = get_postcode(item.y, item.x)
        if retVal:
            row['postcode'] = retVal
            print(retVal)
            #dbsource.save(row)

tag_geos()