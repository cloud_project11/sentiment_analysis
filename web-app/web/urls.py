from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.Home.as_view(), name='index'),
    url(r'^gs_scenario$', views.GScenario.as_view(), name='gs_scenario'),
    url(r'^fs_scenario$', views.FScenario.as_view(), name='fs_scenario'),
    url(r'^ps_scenario$', views.PScenario.as_view(), name='ps_scenario'),
    url(r'^xs_scenario$', views.XScenario.as_view(), name='xs_scenario'),
    url(r'^about$', views.About.as_view(), name='about'),
]