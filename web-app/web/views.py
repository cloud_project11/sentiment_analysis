from django.views.generic import ListView
from .models import GScenarioModel, FScenarioModel, PScenarioModel, XScenarioModel

class Home (ListView):
    queryset = []
    context_object_name = 'info'

    template_name = 'web/index.html'

class About (ListView):
    queryset = []
    context_object_name = 'info'

    template_name = 'web/about.html'

class GScenario (ListView):
    queryset = GScenarioModel.data_manager.all()
    context_object_name = 'info'

    template_name = 'web/gs_scenario.html'


class FScenario(ListView):
    queryset = FScenarioModel.data_manager.all()
    context_object_name = 'info'

    template_name = 'web/fs_scenario.html'


class PScenario(ListView):
    queryset = PScenarioModel.data_manager.all()
    context_object_name = 'info'

    template_name = 'web/ps_scenario.html'


class XScenario(ListView):
    queryset = XScenarioModel.data_manager.all()
    context_object_name = 'info'

    template_name = 'web/xs_scenario.html'




