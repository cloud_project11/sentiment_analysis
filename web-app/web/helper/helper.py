import pickle
import os

pickle_directory = 'pickled_data/'

def get_from_pickle(pickle_name):
    pickle_path = pickle_directory + pickle_name
    if os.path.isfile(pickle_path):
        pickle_file = open(pickle_path, "rb")
        object = pickle.load(pickle_file)
        pickle_file.close()
        return object
    return False

def save_pickle(object, pickle_name):
    pickle_path = pickle_directory + pickle_name
    pickle_file = open(pickle_path, "wb")
    pickle.dump(object, pickle_file)
    pickle_file.close()