from django.db import models
import web.scenarios.green_scenario as gs
import web.scenarios.family_scenario as fs
import web.scenarios.psychological_scenario as ps
import web.scenarios.x_project_scenario as xs
import json

class GScenarioManager(models.Manager):

    def get_queryset(self):

        postcode_info = gs.get_postcode_data()

        results = {
            'postcode_info': json.dumps(postcode_info)
        }

        return results


class GScenarioModel (models.Model):
    data_manager = GScenarioManager()

class FScenarioManager(models.Manager):

    def get_queryset(self):
        postcode_info = fs.get_postcode_data()

        results = {
            'postcode_info': json.dumps(postcode_info)
        }

        return results

class FScenarioModel(models.Model):
    data_manager = FScenarioManager()


class PScenarioManager(models.Manager):
    def get_queryset(self):
        postcode_info = ps.get_postcode_data()

        results = {
            'postcode_info': json.dumps(postcode_info)
        }

        return results

class PScenarioModel(models.Model):
    data_manager = PScenarioManager()


class XScenarioManager(models.Manager):
    def get_queryset(self):
        data, words = xs.get_data()

        results = {
            'data': json.dumps(data),
            'words': json.dumps(words)
        }

        print(words[:100])

        return results

class XScenarioModel(models.Model):
    data_manager = XScenarioManager()
