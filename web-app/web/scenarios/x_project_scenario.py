import web.helper.helper as helper
import couchdb
import string
import nltk

def get_data():
    pickle_name = 'xs_postcode_info.pickle'
    cached = helper.get_from_pickle(pickle_name)
    if cached:
        return cached

    couch = couchdb.Server('http://115.146.93.17:5984')
    dbsource = couch['aus_dev3']
    # L = dbsource.view('_design/grouping/_view/user_postcode', reduce=True, group=True)
    L = dbsource.view('_design/grouping/_view/user_postcode', reduce=False)

    translator = str.maketrans({key: None for key in string.punctuation})

    user = dict()
    c = 0
    for row in L:
        key = row['key']
        value = row['value']
        if key[0] not in user:
            user[key[0]] = {"pos":0, "neu":0, "neg":0, "postcodes":dict(), "len":0, "words":dict(), "coords":[] }
        user[key[0]]['pos'] += value[0]
        user[key[0]]['neu'] += value[1]
        user[key[0]]['neg'] += value[2]
        user[key[0]]['postcodes'][key[1]] = 1

        user[key[0]]['len'] = len(user[key[0]]['postcodes'])

        nouns = []

        words = value[3].split()
        pos_tags = nltk.pos_tag(words)
        for pos_t in pos_tags:
            word = pos_t[0]
            if len(word) <= 3 or pos_t[1][0] not in ['N']:
                continue
            if word not in user[key[0]]['words']:
                user[key[0]]['words'][word] = 0
            user[key[0]]['words'][word] += 1
            iw = word.translate(translator)
            nouns.append(iw.lower())
        sent = 'pos'
        if value[1] == 1:
            sent = 'neu'
        elif value[2] == 1:
            sent = 'neg'

        value[4]['coordinates'].append(sent)
        value[4]['coordinates'].append(nouns)
        user[key[0]]['coords'].append(value[4]['coordinates'])

        c += 1

        if c % 100 == 0:
            print("Processed: %d" % c)

    wordcloud = dict()
    sv = sorted(user, key=lambda id: user[id]['len'], reverse=True)
    for key in sv:
        if user[key]['len'] <= 1:
            break

        # sort the words and print out
        sortedwords = sorted(user[key]['words'].items(), key=lambda x:x[1], reverse=True)
        #print(key, user[key]['pos'], user[key]['neu'], user[key]['neg'], user[key]['len'], sortedwords)

        #take top N words and put it in word cloud
        N = 3
        #sortedwords = sortedwords[:N]
        for word in sortedwords:
            w = word[0].lower()
            if w not in wordcloud:
                wordcloud[w] = 0
            wordcloud[w] += 1

    #sort wordclouds and print
    sortedwordcloud = sorted(wordcloud.items(), key=lambda x:x[1], reverse=True)
    print(sortedwordcloud)

    to_delete = []
    for k, u in user.items():
        # u['coords'].sort()
        # u['coords'] = list(k for k, _ in itertools.groupby(u['coords']))

        total = u['pos'] + u['neu'] + u['neg']

        u['pos'] = (u['pos'] / total) * 100
        u['neu'] = (u['neu'] / total) * 100
        u['neg'] = (u['neg'] / total) * 100

        if u['len'] < 15 or k == "733961780":
            to_delete.append(k)

    for td in to_delete:
        del user[td]

    helper.save_pickle((user, sortedwordcloud), pickle_name)
    return user, sortedwordcloud

