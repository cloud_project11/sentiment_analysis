import csv
import web.helper.postcodes as postcodes
import web.helper.helper as helper
import ast
import pickle
import os
import couchdb

asset_types = [
    "Barbeque",
    "Bicycle Rails",
    "Bin Corral",
    "Bollard",
    "Drinking Fountain",
    "Floral Crate/Planter Box",
    "Hoop",
    "Horse Trough",
    "Information Pillar",
    "Litter Bin",
    "Picnic Setting",
    "Seat",
    "Tree Guard",
]

def in_health_corpus(text):
    f = open('../terms_fitness.txt', 'r').read()
    for term in f.split("\n"):
        if term in text:
            print(term)
            return True
    return False

def get_postcode_data():

    pickle_name = 'gs_postcode_info.pickle'
    cached = helper.get_from_pickle(pickle_name)
    if cached:
        return cached

    postcode_info = {

    }
    with open('../csvfiles/Outdoor_Furniture.csv', newline='') as csvfile:
        out_furs = csv.DictReader(csvfile, delimiter=',', quotechar='"')
        for of in out_furs:
            coords = ast.literal_eval(of['CoordinateLocation'])
            postcode = str(postcodes.get_postcode(coords[0], coords[1]))
            if postcode not in postcode_info:
                postcode_info[postcode] = { asset_t: 0 for asset_t in asset_types }
            postcode_info[postcode][of['ASSET_TYPE']] += 1

    server = couchdb.Server('http://115.146.93.17:5984')
    db = server['aus_dev3']

    for post_sent in db.view('grouping/sentiment_by_postcode_gs', group=True):
        total = sum(post_sent.value)
        postcode_info[post_sent.key]['positive'] = int((post_sent.value[0] / total) * 100)
        postcode_info[post_sent.key]['negative'] = int((post_sent.value[2] / total) * 100)

    for t in db.view('filtered/has_postcode'):
        pc = t.value['postcode']
        if pc in postcode_info:
            if 'fitness_count' not in postcode_info[pc]:
                postcode_info[pc]['fitness_count'] = 0
            postcode_info[pc]['fitness_count'] += 1

    helper.save_pickle(postcode_info, pickle_name)
    return postcode_info
