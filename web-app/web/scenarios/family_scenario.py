import csv
import web.helper.postcodes as postcodes
import web.helper.helper as helper
import ast
import couchdb

def get_postcode_data():

    pickle_name = 'fs_postcode_info.pickle'
    cached = helper.get_from_pickle(pickle_name)
    if cached:
        return cached

    postcode_info = {}
    with open('../aurin_marital_status_output_revised.csv', newline='') as csvfile:
        out_furs = csv.DictReader(csvfile, delimiter=',', quotechar='"')
        for of in out_furs:
            postcode = str(of['postcode'])
            postcode_info[postcode] = {
                'married_percent': of['married_percent'],
                'never_married_percent': of['never_married_percent'],
                'divorced_percent': of['divorced_percent'],
                'other_percent': of['other_percent'],
            }

    server = couchdb.Server('http://115.146.93.17:5984')
    db = server['aus_dev3']

    for post_sent in db.view('grouping/sentiment_by_postcode2', group=True):
        total = sum(post_sent.value)
        if post_sent.key not in postcode_info:
            postcode_info[post_sent.key] = {}
        postcode_info[post_sent.key]['positive'] = int((post_sent.value[0] / total) * 100)
        postcode_info[post_sent.key]['negative'] = int((post_sent.value[2] / total) * 100)

    helper.save_pickle(postcode_info, pickle_name)
    return postcode_info