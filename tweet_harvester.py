import couchdb
from tweepy import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from http.client import IncompleteRead
from requests.packages.urllib3.exceptions import ProtocolError
import time
import json
import sys
from process_tweet import get_sentiment

s = 0

server1 = {
    # ds_amores credentials "Australian Cities Analytics"
    'credentials': {
        'ckey': "J54IvWlNrWI1qtYr1fZGLkpyE",
        'csecret': "FGhBHWbZEycxcQNhMreu9qB78IH3T4uyhnMJPEZn8LZYvNXt6m",
        'atoken': "218523248-WAJ7LDS5YNgKXRousUssdD9eENIRNXybetUC8XR2",
        'asecret': "AUnz3FKIwtZqpUDIYGTwp0ouMlGOFlT5J9FIKEjF9cVOZ"
    },
    'coords': [144.402136, -38.086704, 144.869945, -37.466856] # left melb
}

server2 = {
    # vero_tp credentials "My second app for Australia"
    'credentials': {
        'ckey': "tmP1sWgUxxbfJhnO2LJpenX5w",
        'csecret': "C68Dc9o43jHgKAfsUr0crmii3uwVlycly2hDy0Twy7WUuU3x9C",
        'atoken': "67933571-9UdDS24JJnOLpRgM6zuFFn2tM7p1xBXZC384ryElt",
        'asecret': "ii1e4v4xfnr40OoNfvrIr0EQPhgnU88dWjtFZiuS5x6RW"
    },
    'coords': [144.786911, -37.947082, 145.050327, -37.466856] # center melb
}

server3 = {
    # Tri's credentials
    'credentials': {
        'ckey': "xC64fK56GLOpvTRLh4kAXvLpa",
        'csecret': "BaNEC5k2I2BFddExCXRBn3a1jdf5HIZUmpXa5zgD2bV4tEGzZa",
        'atoken': "727484492030468096-QlChc8EOyY40cycioDXL5PCDKblrsjJ",
        'asecret': "mWUlPqK0EO3lJhcfeblBQ3oecu9T2mGDzNhWrORW9g1Fd"
    },
    'coords': [144.975884, -38.208528, 145.760405, -37.480490] # right melb
}

servers = [
    server1,
    server2,
    server3
]

server = servers[s]

db_name = sys.argv[1]#'aus_analytics'

couch = couchdb.Server(sys.argv[2])#'http://115.146.93.17:5984')
db = couch[db_name]

class MyListener(StreamListener):

    def on_data(self, raw_data):
        json_data = json.loads(raw_data)
        json_data["_id"] = json_data["id_str"]
        json_data['type'] = 'tweet'
        json_data["sentiment"] = get_sentiment(json_data["text"])
        while True:
            try:
                db.save(json_data)
                break
            except couchdb.http.ResourceConflict:
                break
            except couchdb.http.ServerError as e:
                print("Server error %s. Sleeping for 60 seconds" % e)
                sys.stdout.flush()
                time.sleep(60)
        return True

    def on_error(self, status):
        print("Error: " + str(status))
        sys.stdout.flush()

auth = OAuthHandler(server['credentials']['ckey'], server['credentials']['csecret'])
auth.set_access_token(server['credentials']['atoken'], server['credentials']['asecret'])

print("start")
sys.stdout.flush()

while True:

    try:
        twitterStream = Stream(auth, MyListener())
        twitterStream.filter(locations=server['coords'], languages=['en'])
    except (IncompleteRead, ProtocolError, AttributeError):
        print("Twitter Error: Sleeping for 5 seconds")
        sys.stdout.flush()
        time.sleep(5)
        continue
    except KeyboardInterrupt:
        twitterStream.disconnect()
        break
    except Exception as e:
        print("Unknown Error %s: Sleeping for 10 seconds - " % e)
        sys.stdout.flush()
        time.sleep(10)
    sys.stdout.flush()
