#!/usr/bin/env python

#========================
# Author: GROUP 11
#
# SENTIMENTAL ANALYSIS
# Date: May 12, 2016.
#=========================

# Sources from the tutorial slides and the web page
import boto
import boto.ec2
import time
from boto.ec2.regioninfo import RegionInfo
import configparser
import os
import stat
import sys

# Establish the region to NeCTAR at Melbourne
region = RegionInfo(name='melbourne', endpoint='nova.rc.nectar.org.au')

# Create a connection between PC and NeCTAR using Access+private key
ec2_conn = boto.connect_ec2(aws_access_key_id='7f30f692d13e4f2faf21b3c8fdc08da7',
aws_secret_access_key='388eabd23a9b4a599b2d1b06d78a8a71', is_secure=True,
region=region, port=8773, path='/services/Cloud', validate_certs=False)

# Creation of the instances
num_inst = 1
key_name='test'

# Verify the key_file
try:
    key = ec2_conn.get_all_key_pairs(keynames=[key_name])[0]
    # print(key)
except ec2_conn.ResponseError as e:
    if e.code == 'InvalidKeyPair.NotFound':
        print('Creating keypair: %s' % key_name)
        # Create an SSH key to use when logging into instances.
        key = ec2_conn.create_key_pair(key_name)

        # Make sure the specified key_dir actually exists.
        # If not, create it.
        key_dir = os.expanduser(key_dir)
        key_dir = os.expandvars(key_dir)
        if not os.path.isdir(key_dir):
            os.mkdir(key_dir, '0700')

        # AWS will store the public key but the private key is
        # generated and returned and needs to be stored locally.
        # The save method will also chmod the file to protect
        # your private key.
        key.save(key_dir)
        print(key)
        print(key_dir)
    else:
        raise

for i in range(num_inst):
    newreservation = ec2_conn.run_instances('ami-00003843',
                                            key_name=key_name,
                                            placement='melbourne-np',
                                            instance_type='m1.small',
                                            security_groups=['Cloud'])
# # # Assign the instances
reservations = ec2_conn.get_all_reservations()
#
# #Show reservation details
# print('Wait please...')
# time.sleep(60)
for idx, res in enumerate(reservations):
    print (idx, res.id, res.instances[0].ip_address)
print (reservations[0].instances[0].ip_address)

#
# #Create a volume
# vol_req = ec2_conn.create_volume(50, "melbourne-np")
# print (vol_req.id)
#
# #Check provisioning status:
# # for i in range(num_inst):
# curr_vol = ec2_conn.get_all_volumes([vol_req])[0]
# print (curr_vol.status)
# print (curr_vol.zone)
#
# while (reservations[0].instances[0].status != 'Active') or (reservations[0].instances[0].status != 'Available') :
#     time.sleep( 60 )
#
# #Attach volume
# ec2_conn.attach_volume (vol_req.id, reservations[0].instances[0].id, "/dev/vdc")
# print ("Attached Volume Success")
#============================

BASE_FILE = os.path.dirname(os.path.abspath(__file__))
# FOLDER_CONTAINER = os.path.join(BASE_FILE, './group_11')

# if not os.path.exists(FOLDER_CONTAINER):
#     os.makedirs(FOLDER_CONTAINER)

ANSIBLE_HOSTS_FILENAME = './hosts.ini'
ANSIBLE_HOSTS_FILEPATH = os.path.join(BASE_FILE, ANSIBLE_HOSTS_FILENAME)

# Change the path
pem_file_path = 'test-2.pem'#'/Users/Carina/Desktop/Proy_2/test-2.pem'
pem_file_path = os.path.expanduser(pem_file_path)  # expand ~user

if os.path.isdir(pem_file_path):
    print ("You have entered a directory, but no file name: {0}".format(
        pem_file_path))
    sys.exit(1)

if not os.path.exists(pem_file_path):
    print ("Nope. This file cannot be found: {0}".format(pem_file_path))
    sys.exit(1)

user = 'ubuntu'
user = user.strip()

config = configparser.RawConfigParser(allow_no_value=True)
config.add_section('database')
config.add_section('webserver')
config.add_section('webserver:vars')
config.add_section('database:vars')
config.set('database:vars', 'ansible_ssh_private_key_file', pem_file_path)
config.set('database:vars', 'ansible_port', '22')
config.set('database:vars', 'ansible_user', user)
config.set('database', reservations[0].instances[0].ip_address)
config.set('webserver', reservations[1].instances[0].ip_address)

with open(ANSIBLE_HOSTS_FILENAME, 'w') as config_file:
        config.write(config_file)

# Print the data of the images availables for the creation of the instaces
# # images = ec2_conn.get_all_images()
# # for img in images:
# #     print ('id: ', img.id, 'name: ', img.name)
#



