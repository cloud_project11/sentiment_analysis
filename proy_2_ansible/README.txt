
README
======
After the analysis of the files and before to start the running, the PC should have installed the
ansible dependencies for the corresponding operative system. After that, follow the next steps:

1. Check that the files: key.pem, instance.py, db.yml, and web.yml are in the same path. 
2. Using the cmd locate the folder.
3. To run instance.py use: sudo python3 instance.py
4. Review if the instance is created and running on the Nectar Cloud.
5. To run db.yml use the command: ansible-playbook -i hosts.ini db.yml. It is important to consider that the file host.ini is created 	  automatically after the running the item 3. This file should be located in the same folder of the other files.
6. The web.yml file runs: ansible-playbook -i hosts.ini web.yml. This script uses the same host.ini file.
7. Test the database and web application using the following commands:
	Database: http://host_ip/_util/index.html
	Web Application: http://hos_ip:5000/web/about