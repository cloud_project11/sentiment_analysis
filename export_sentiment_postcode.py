import couchdb
import csv

def connection():
	couch = couchdb.Server('http://115.146.93.17:5984/')
	db = couch['aus_dev2'] # existing
	return couchdb_pager(db)
	# for doc in couchdb_pager(db):
		# print(doc['key'], doc['value'])

def couchdb_pager(db, view_name='_design/grouping/_view/sentiment_by_postcode2', startkey=None, startkey_docid=None, endkey=None, endkey_docid=None, bulk=5000):
	# Request one extra row to resume the listing there later.
	options = {'limit': bulk + 1}
	if startkey:
		options['startkey'] = startkey
		if startkey_docid:
			options['startkey_docid'] = startkey_docid
	if endkey:
		options['endkey'] = endkey
		if endkey_docid:
			options['endkey_docid'] = endkey_docid
	done = False
	while not done:
		view = db.view(view_name, group = True)
		rows = []
		# If we got a short result (< limit + 1), we know we are done.
		if len(view) <= bulk:
			done = True
			rows = view.rows
		else:
			# Otherwise, continue at the new start position.
			rows = view.rows[:-1]
			last = view.rows[-1]
			options['startkey'] = last.key
			options['startkey_docid'] = last.id

		for row in rows:
			yield row

def processData(data):
	infoList = []
	for doc in data:
		tmpDictInner = {}
		tmpDictInner['postcode'] = doc['key']
		tmpDictInner['positive'] = doc['value'][0]
		tmpDictInner['neutral'] = doc['value'][1]
		tmpDictInner['negative'] = doc['value'][2]
		infoList.append(tmpDictInner)
	return infoList

def outputCSV(infoList):
	csv_columns = ['postcode', 'positive', 'negative', 'neutral']
	with open('csvfiles/postcode_sentiment.csv', 'w') as csvfile:
		writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
		writer.writeheader()
		for data in infoList:
			writer.writerow(data)

outputCSV(processData(connection()))