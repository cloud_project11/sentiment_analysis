from textblob.classifiers import NaiveBayesClassifier
import csv

def readCSV(file):
	allLines = []
	with open(file, 'rU') as f:
		reader = csv.reader(f)
		for line in reader:
			allLines.append(tuple(line))
	return allLines

def train():
	train = readCSV('traindata.csv')
	cl = NaiveBayesClassifier(train)
	return cl

def tag(cl, text):
	return cl.classify(text)


def test():
	cl = train()
	print cl.classify(":)")
	print cl.classify(":(")
	print cl.classify("if it is possible :-(")
	prob_dist = cl.prob_classify("It is really bad")
	print prob_dist.max()
	print round(prob_dist.prob("pos"), 2)
	print round(prob_dist.prob("neu"), 2)
	print round(prob_dist.prob("neg"), 2)

test()
