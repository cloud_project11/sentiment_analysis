from tweepy import OAuthHandler
import tweepy
import time
import couchdb
import sys
from process_tweet import get_sentiment
import csv
import ast
from geopy.distance import vincenty

s = 0

def get_geos():
    geos = []
    with open('postcodes.csv', newline='') as csvfile:
        areas = csv.DictReader(csvfile, delimiter=',', quotechar='"')
        for area in areas:
            coords = ast.literal_eval(area['boundedBy'])
            center = ((coords[0] + coords[2]) / 2, (coords[1] + coords[3]) / 2)
            dist = vincenty(center, (coords[0], coords[1])).kilometers
            geo = "%s,%s,%skm" % (center[1], center[0], dist)
            geos.append(geo)
    return geos

geos = get_geos()

server1 = {
    # ds_amores credentials "Second Australian Cities"
    'credentials': {
        'ckey': "CzmVaiTpCs7UdqeH28rnzDaZu",
        'csecret': "6q3Isi4pm8Y1NyPuW0sGKIp0HiTu1KYYACpUeedgBeUsFrtL52",
        'atoken': "218523248-buX14uhApOb7Ei8rqPvicb6wdZd9wpM7w7suutcQ",
        'asecret': "ZDlkKSAOIbWipSzLpEAFXbllBavJyKis9hjwrctehMPwP"
    },
    'geos': geos # left melb
}

server2 = {
    # Angela's credentials
    'credentials': {
        'ckey': "IswkdNrhcsauRsBK8esfX1ko3",
        'csecret': "8lYmGMCaHA9rRZI6t81v5dcCjIswrqYyIo2nwIXiHlUdky9EoT",
        'atoken': "3186106424-ivw3HRIQRMHHgPMbkkVmqdwlaNdGWTyF1wRWz2y",
        'asecret': "bUU6bwSpqIxPHdrSs235s74FJp7jzWqNQpOv1gYe6wAEu"
    },
    'geos': geos # center melb
}

server3 = {
    # Janice's credentials
    'credentials': {
        'ckey': "H9TfC3iITRDlxCNUY5wqvpPNy",
        'csecret': "QHSQ00xnWURf0lKNPyZ0gIEimjI3eWigYG6yid3r4ML5V4ZEbQ",
        'atoken': "2843502054-TmJMKXmMAttbaAwkEPpM2WQ9Blsv8vylwe8Quu9",
        'asecret': "0Bt1sddXkhNH727EUUj94OsOrCnuwNS9F8FIJWKtRaeRB"
    },
    'geos': geos # right melb
}

servers = [
    server1,
    server2,
    server3
]

server = servers[s]

db_name = 'aus_analytics'

couch = couchdb.Server('http://115.146.93.17:5984')
db = couch[db_name]

class TweetProcessor:

    def __init__(self, keyword=None):
        self.keyword = keyword

    def process_tweet(self, status):
        json_tweet = status._json
        json_tweet["_id"] = json_tweet["id_str"]
        json_tweet["sentiment"] = get_sentiment(json_tweet["text"])
        if self.keyword == '':
            json_tweet["type"] = 'tweet'
        else:
            json_tweet["type"] = 'tweet_search'
            json_tweet["matched"] = self.keyword
        while True:
            try:
                db.save(json_tweet)
                return True
            except couchdb.http.ResourceConflict:
                return False
            except couchdb.http.ServerError as e:
                print("Server error %s. Sleeping for 60 seconds" % e)
                sys.stdout.flush()
                time.sleep(60)

    def process_tweets(self, results):
        last_id = int(results[0].id_str)
        processed = 0
        for res in results:
            if tp.process_tweet(res):
                processed += 1
            if int(res.id_str) < last_id:
                last_id = int(res.id_str)
        return last_id, processed


auth = OAuthHandler(server['credentials']['ckey'], server['credentials']['csecret'])
auth.set_access_token(server['credentials']['atoken'], server['credentials']['asecret'])
api = tweepy.API(auth)

print("start")
sys.stdout.flush()

terms_files = ['terms1.txt', 'terms2.txt', 'terms3.txt', 'terms4.txt', 'terms5.txt', 'terms6.txt']

i = s

while True:

    file_name = terms_files[i % len(terms_files)]

    f = open(file_name, 'r').read()

    keywords = []

    for kw in f.split('\n'):
        keywords.append(kw.strip())

    for kw in keywords:
        tp = TweetProcessor(kw)
        for geo in server['geos']:
            count = 0
            while True:
                try:
                    results = api.search(kw, geocode=geo, lang='en', count=100)
                    break
                except tweepy.error.RateLimitError:
                    print("1. Rate limit error: sleeping for 1000 seconds")
                    sys.stdout.flush()
                    time.sleep(1000)
                except KeyboardInterrupt:
                    exit()
                except Exception as e:
                    print("1. Unknown Error %s - Sleeping for 1000 seconds " % e)
                    sys.stdout.flush()
                    time.sleep(1000)

            while len(results) > 0:
                last_id, processed = tp.process_tweets(results)
                count += processed
                sys.stdout.flush()
                while True:
                    try:
                        results = api.search(kw, geocode=geo, lang='en', count=100, max_id=last_id)
                        break
                    except tweepy.error.RateLimitError:
                        print("2. Rate limit error: sleeping for 1000 seconds")
                        sys.stdout.flush()
                        time.sleep(1000)
                    except KeyboardInterrupt:
                        exit()
                    except Exception as e:
                        print("2. Unknown Error %s - Sleeping for 1000 seconds" % e)
                        sys.stdout.flush()
                        time.sleep(1000)
                results.pop(0)
            print("Collected %d for word '%s' in geo '%s'" % (count, kw, geo))
            sys.stdout.flush()
    print("Collected for every keyword in %s, sleeping for 4000 secs" % file_name)
    sys.stdout.flush()
    time.sleep(4000)
    print("Woke up! keep collecting...")
    sys.stdout.flush()
    i += 1