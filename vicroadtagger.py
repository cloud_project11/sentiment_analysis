import couchdb
import shapefile
import json


def inPolygonCheck(polys, test):
    retVal = False
    i=0
    j=len(polys)-1

    while i<len(polys):
        if ( ((polys[i][1]>=test.y) != (polys[j][1]>=test.y)) and (test.x<=(polys[j][0]-polys[i][0])*(test.y-polys[i][1])/(polys[j][1]-polys[i][1])+polys[i][0]) ):
            retVal = not retVal
        j = i
        i += 1

    return retVal

# a = [[1, 20], [10, 20], [10, 3], [1, 3]]
# b = type('Point', (object,), {"x": 10, "y": 10})
# print(inPolygonCheck(a, b))

# sf = shapefile.Reader("blackspots/98a94386-6975-42b4-97db-bc09f3a9805c")
# sf = shapefile.Reader("data/Victorian_Features_of_Interest__Polygon__-_Vicmap_FOI/shp/242af68e-b541-4548-ad12-deb4f2102f57")
f = open('data/Victorian_Road_Traffic_Volumes.json/data2412558373899220745.json', 'r')

while 1:
    line = f.readline()
    if not line:
        break

    d = json.loads(line)

    # print (d['features'][0]['geometry'].keys())
    # print (d['features'][0]['properties'].keys())
    # print()

# (sport facility:*), (reserve:park, city square), (recreational resource:*)
    polys = []
    for geom in d['features']:
        temp = type('Polygon', (object,), { "prop": [], "coor":[] })
        temp.prop = geom['properties']
        temp.coor = geom['geometry']['coordinates'][0]
        polys.append(temp)

couch = couchdb.Server('http://115.146.93.17:5984')
dbsource = couch['aus_dev2']

items = []
# for row in L:
for id in dbsource:
    row = dbsource[id]
    # print(row['sentiment'])
    item = type('Point', (object,), {"x": 0, "y": 0, "sentiment": 0, "properties": "unknown"})
    # item.x = row['value'][0]['coordinates'][1]
    # item.y = row['value'][0]['coordinates'][0]
    if ('geo' in row and row['geo'] is not None):
        item.x = row['geo']['coordinates'][1]
        item.y = row['geo']['coordinates'][0]
    if ('sentiment' in row):
        item.sentiment = row['sentiment']
    else:
        print(row)
        continue
    for poly in polys:
        retVal = inPolygonCheck(poly.coor, item)
        if retVal:
            item.properties = str(poly.prop)
            break
    items.append(item)

itemgrouppos = dict()
itemgroupneg = dict()
itemgroupneut = dict()
countneut = 0
countpos = 0
countneg = 0
for item in items:
    if item.properties not in itemgrouppos:
        itemgrouppos[item.properties] = 0
        itemgroupneg[item.properties] = 0
        itemgroupneut[item.properties] = 0
    if item.sentiment == 'negative':
        itemgroupneg[item.properties] += 1
        countneg += 1
    elif item.sentiment == 'neutral':
        itemgroupneut[item.properties] += 1
        countneut += 1
    else:
        itemgrouppos[item.properties] += 1
        countpos += 1

for key in itemgrouppos.keys():
    print (key, 'pos', itemgrouppos[key], 'neg', itemgroupneg[key], 'neut', itemgroupneut[key])
print('pos', countpos, 'neg', countneg, 'neut', countneut)




